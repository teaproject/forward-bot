""" Distributed under GNU Affero GPL license.
    
    Forward Bot v2.0 01/08/2019.

    Copyright (C) 2019  Davide Leone

    You can contact me at leonedavide[at]protonmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

from config import *

#--- FILE-RELATED FUNCTIONS ---
def get_ids():
    #This function reads the .txt file with all the ids
    with open(ids_file,'r') as file:
        ids = file.read()
        ids = ids.split()
        
    return ids


def add_id(chat_id):
    #Add an id to the file
    ids = get_ids()

    if not(chat_id in ids):
        
        with open(ids_file,'a') as file:
            file.write("\n"+chat_id)
            
        return True

    return False

#--- CHANNEL-RELATED FUNCTIONS ---
def group_callback(update, context):
    #The group is added to the id_file
    chat_id = str(update.message.chat_id)

    if add_id(chat_id) and (update.message.migrate_from_chat_id == None):
        message = "Thanks for adding me to the group!\nI will forward here all the messages from {}!".format(channel_name)
        update.message.reply_html(text = message)

def channel_callback(update, context):
    #Forward a new post from the channel to every group in the ids list
    ids = get_ids()

    for _id in ids:
        try:
            update.channel_post.forward(_id)
        except telegram.error.Unauthorized:
            None
        except telegram.error.ChatMigrated:
            None
        except telegram.error.BadRequest:
            None

#--- USERS-RELATED FUNCTIONS ---

def private_callback(update, context):
    #A message is sent in private chat in response to everything       
    message = "Welcome. This is the bot of the channel {channel}.\n\nIf you <b>add me to a group</b> or a channel, <b>I wil forward there all posts made by the channel!</b>".format(channel=channel_name)

    keyboard = InlineKeyboardMarkup(
        [[
            InlineKeyboardButton(
                text = 'Add me to a group!',
                url = 't.me/{botusername}?startgroup=start'.format(botusername=bot.username)
                )
            ]]+
        [[
            InlineKeyboardButton(
                text = 'Source code',
                url = source_url
                )
            ]]
        )

    update.message.reply_html(text = message, reply_markup = keyboard, disable_web_preview = True)

#--- HANDLERS AND POLLING --- 
channel_handler = MessageHandler(
    callback = channel_callback,
    filters = (Filters.chat(chat_id = channel_id))
    )

group_handler = MessageHandler(
    callback = group_callback,
    filters = (Filters.group)
    )

private_handler = MessageHandler(
    callback = private_callback,
    filters = (Filters.private)
    )

handlers = [channel_handler, group_handler, private_handler]

for handler in handlers:
    dispatcher.add_handler(handler)
    
updater.start_polling()
updater.idle() 

