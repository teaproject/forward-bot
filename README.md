# Forward Bot

**Forward Bot** is a Telegram bot that will forward new posts from your channel to every group where the bot is present.

## Why Forward Bot

This bot is very helpful in channel administration. It's main function is to **spread new posts of your channel everywhere it can**. This way, your channel may expand seriously it's views, as **a lot more users will see your posts** and, this way, **your subscriptions will increase**. 
Everyone of your subscribers can, with a few simple and intuitive clicks, add the bot to a new group. 

If you think your users may be interested in doing so, then this bot may be a huge addition to your channel. Not every channel will benefit from this kind of activity, as users may just not be interested in adding the bot to their groups. 

## How was developed

This bot has been developed in python3 with the [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) library v12. 

# Contributors:
- [Davide Leone](telegram.me/davideleone) - <leonedavide[at]protonmail.com>
- Giovanni Zaccaria

[Icon](https://www.flaticon.com/free-icon/email_186060#term=send&page=2&position=70) made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a></div>