""" Distributed under GNU Affero GPL license.

    Configuration file for ForwardBot v2.0 01/08/2019.

    Copyright (C) 2019  Davide Leone

    You can contact me at leonedavide[at]protonmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#--- LIBRARIES IMPORT ---
import html
import logging
from pathlib import Path
import telegram
from telegram.ext import Updater, MessageHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

#--- VARIABLES DEFINITION ---
api_token = ''
ids_file = 'ids.txt' 
channel_id = -1001245511055
source_url = 'https://gitlab.com/tea-project/forward-bot'

updater = Updater(token=api_token, use_context=True)
dispatcher = updater.dispatcher
bot = updater.dispatcher.bot

#--- GET THE CHANNEL NAME FORMATTED WITH A LINK (OR THE USERNAME) ---
channel_info = bot.getChat(channel_id)
if channel_info.username == None:
    bot.exportChatInviteLink(channel_id)
    channel_name = "<a href='"+channel_info.invite_link+"'>"+html.escape(channel_info.title)+"</a>"
else:
    channel_name = "@"+channel_info.username

#--- LOGGING CONFIGURATION ---
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

#--- NEEDED FILES CHECK ---
id_list = Path(ids_file)
if not id_list.is_file(): #The ids_file was not created yet
    open(ids_file,'w')